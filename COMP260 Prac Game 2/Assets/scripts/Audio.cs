﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class Audio : MonoBehaviour {
	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public AudioClip goalClip;
	public LayerMask paddleLayer;

	private AudioSource audio;

	void Start(){
		audio = GetComponent<AudioSource> ();
		Goal[] goals = FindObjectsOfType<Goal> ();
		for (int i = 0; i < goals.Length; i++) {
			goals[i].ClipGoalEvent += OnGoal;
		}

		PuckControl[] collision = FindObjectsOfType<PuckControl> ();
		for (int i = 0; i < collision.Length; i++) {
			collision[i].ClipCollEvent += OnColl;
		}	
	}
	public void OnGoal(){
		audio.PlayOneShot (goalClip);
	}
	public void OnColl(Collision coll){
	
		if (paddleLayer.Contains (coll.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}

	}

}
