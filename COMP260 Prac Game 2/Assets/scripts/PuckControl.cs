﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

//	public AudioClip wallCollideClip;
//	public AudioClip paddleCollideClip;
//	public LayerMask paddleLayer;
//	private AudioSource audio;

	public Transform startingPos;
	private Rigidbody rigidbody;

	public delegate void ClipCollHandler(Collision coll);
	public ClipCollHandler ClipCollEvent;

	void Start () {
//		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition ();

	}
	public void ResetPosition(){
		rigidbody.MovePosition (startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}
	void OnCollisionEnter(Collision collision){
//		if (true) {
//			audio.PlayOneShot (paddleCollideClip);

//		} else {
//			audio.PlayOneShot (wallCollideClip);

//		}
		if (ClipCollEvent != null) {
			ClipCollEvent(collision);
		}
	}
}

