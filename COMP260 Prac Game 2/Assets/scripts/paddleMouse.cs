﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class paddleMouse : MonoBehaviour {
	private Vector3 GetMousePosition() {
		// create a ray from the camera 
		// passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		// find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}
	void OnDrawGizmos() {
		// draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}

	public float maxspeed = 10f;
	public Rigidbody rb;
	void FixedUpdate () { 
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rb.position;
		Vector3 vel = dir.normalized * maxspeed;

		float move = maxspeed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			vel = vel * distToTarget / move;
		}
		rb.velocity = vel;
	}

}
