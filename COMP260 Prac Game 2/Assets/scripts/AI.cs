﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class AI : MonoBehaviour {

	public Rigidbody paddle;
	public Transform puck;
	public Transform goalKeeper;
	public float maxspeed = 5.0f;

	private Vector3 getAiPosition(){
		if(puck.position.x > -1 && puck.position.x < paddle.position.x)
        {
			Vector3 attack = puck.position;
			return attack;
		}
		Vector3 defence = (puck.position + goalKeeper.position)/2;
		return defence;
	}
	void FixedUpdate () { 
		Vector3 pos = getAiPosition ();
		Vector3 dir = pos - paddle.position;
		Vector3 vel = dir.normalized * maxspeed;

		float move = maxspeed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			vel = vel * distToTarget / move;
		}
		paddle.velocity = vel;
	}
	// Update is called once per frame
}
