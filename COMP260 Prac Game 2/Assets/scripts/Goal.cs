﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {
//	public AudioClip goalclip;
//	private AudioSource audio;

	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;

	public delegate void ClipGoalHandler();
	public ClipGoalHandler ClipGoalEvent;

//	void Start () {
//		GetComponent<AudioSource>() = GetComponent<AudioSource> ();
//	}
		
	void OnTriggerEnter (Collider collider) {
//		audio.PlayOneShot (goalclip);
		if (ClipGoalEvent != null) {
			ClipGoalEvent();
		}
		if (scoreGoalEvent != null) {
			scoreGoalEvent(player);
		}
		PuckControl puck = collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition ();

	}
}
